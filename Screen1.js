import React, {Component} from 'react'
import {View, Text, Alert,StyleSheet } from 'react-native'
import { Link } from 'react-router-native'

class Screen1 extends Component{
    state = {
        username : '',
        password : ''
        
    }

    UNSAFE_componentWillMount(){
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.username && this.props.location.state.password){
            this.setState({username : this.props.location.state.username})
            this.setState({password : this.props.location.state.password})
        } 
    }

    logout = () => {
        this.props.history.replace('/')
    }


    render(){
        return(
            <View style = {{flex:1, backgroundColor: 'red'}}>
                <Text style={{color: 'white'}}> Screen1</Text>
                
                <Text style = {styles.header}>{this.state.username} Username- Password {this.state.password}</Text>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        color: "white",
        fontSize: 20
      }

});

export default Screen1